const modalSettings = [
  {
    modalId: "firstModal",
    settings: {
      header: "Some header for first modal",
      closeButton: true,
      text: "Some text for first modal",
      actions: [
        {
          type: "submit",
          backgroundColor: "blue",
          text: "Confirm",
        },
        {
          type: "cancel",
          backgroundColor: "yellow",
          text: "Cancel",
        },
      ],
    },
  },
  {
    modalId: "secondModal",
    settings: {
      header: "Some header for second modal",
      closeButton: false,
      text: "Some text for second modal",
      actions: [
        {
          type: "submit",
          backgroundColor: "pink",
          text: "Confirm",
        },
        {
          type: "cancel",
          backgroundColor: "red",
          text: "Cancel",
        },
      ],
    },
  },
];

export default modalSettings;
