import React, { Component } from "react";
import Button from "../button/Button";
import classes from "./Modal.module.scss";
import modalSettings from "./modalSettings";

export default class Modal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      settings: {},
    };
  }

  componentDidMount() {
    const modal = modalSettings.find(
      (item) => item.modalId === this.props.modalId
    );
    this.setState({ settings: modal.settings });
  }
  render() {
    const { header, closeButton, text, actions } = this.state.settings;
    return (
      <div className={classes.Modal} onClick={this.props.closeModal}>
        <div
          className={classes.ModalContent}
          onClick={(e) => e.stopPropagation()}
        >
          <div className={classes.ModalHeader}>
            {header && header}
            {closeButton && closeButton && (
              <Button
                text="X"
                background="magenta"
                onClick={this.props.closeModal}
              />
            )}
          </div>
          <div className={classes.ModalBody}>{text && text}</div>
          <div className={classes.ModalFooter}>
            {actions &&
              actions.map((item, index) => (
                <Button
                  backgroundColor={item.backgroundColor}
                  text={item.text}
                  onClick={
                    item.type === "submit" // подтверждение действия или отмена (закрытие окна и обнуление его стайта)
                      ? () => this.props.submit(this.props.data) // только в качестве примера. это можно было сделать на уровне выше.
                      : this.props.closeModal
                  }
                  key={Date.now() + index}
                />
              ))}
          </div>
        </div>
      </div>
    );
  }
}
