import React, { Component } from "react";
import classes from "./Button.module.scss";

export default class Button extends Component {
  render() {
    return (
      <button
        className={classes.Button}
        style={{ background: this.props.background }}
        onClick={this.props.onClick}
      >
        {this.props.text}
      </button>
    );
  }
}
