import React, { Component } from "react";
import classes from "./styles/index.module.scss";
import Modal from "./component/UI/modal/Modal";
import Button from "./component/UI/button/Button";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: {
        visible: false, // переключание статуса окна
        modalId: null, // идентификатор модалки
        submit: null, // функция для работы с данными (если потребуется)
        data: null, // данные для их обработки (если потребуется)
      },
    };
  }

  openModal = (modalId, submit = null, data = null) => {
    this.setState({
      modal: {
        visible: true,
        modalId,
        submit,
        data,
      },
    });
  };

  closeModal = () => {
    this.setState({
      modal: {
        visible: false,
        modalId: null,
        submit: null,
        data: null,
      },
    });
  };

  onSubmitFirstModal = (data = "") => {
    alert(data ? data : "Empty 'data'");
    this.closeModal();
  };

  onSubmitSecondModal = (data = "") => {
    alert(data ? data : "Empty 'data'");
    this.closeModal();
  };

  render() {
    return (
      <div className={classes.App}>
        <Button
          background="darkgrey"
          text="Open first modal"
          onClick={() => {
            this.openModal(
              "firstModal", // идентификатор модального окна
              this.onSubmitFirstModal, // событие клика подтверждения действия (если требуется)
              "Some first modal data" // данные для их дальнейшей обработки (если требуется)
            );
          }}
        />
        <Button
          background="lightblue"
          text="Open second modal"
          onClick={() =>
            this.openModal(
              "secondModal", // идентификатор модального окна
              this.onSubmitSecondModal, // событие клика подтверждения действия
              "Some second modal data" // данные для их дальнейшей обработки
            )
          }
        />
        {this.state.modal.visible && (
          <Modal
            modalId={this.state.modal.modalId}
            submit={this.state.modal.submit}
            data={this.state.modal.data}
            closeModal={this.closeModal}
          />
        )}
      </div>
    );
  }
}

export default App;
